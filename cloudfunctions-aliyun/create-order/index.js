'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var crypto = _interopDefault(require('crypto'));

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var jwt_1 = createCommonjsModule(function (module) {
/*
 * jwt-simple
 *
 * JSON Web Token encode and decode module for node.js
 *
 * Copyright(c) 2011 Kazuhito Hokamura
 * MIT Licensed
 */

/**
 * module dependencies
 */



/**
 * support algorithm mapping
 */
var algorithmMap = {
  HS256: 'sha256',
  HS384: 'sha384',
  HS512: 'sha512',
  RS256: 'RSA-SHA256'
};

/**
 * Map algorithm to hmac or sign type, to determine which crypto function to use
 */
var typeMap = {
  HS256: 'hmac',
  HS384: 'hmac',
  HS512: 'hmac',
  RS256: 'sign'
};


/**
 * expose object
 */
var jwt = module.exports;


/**
 * version
 */
jwt.version = '0.5.6';

/**
 * Decode jwt
 *
 * @param {Object} token
 * @param {String} key
 * @param {Boolean} [noVerify]
 * @param {String} [algorithm]
 * @return {Object} payload
 * @api public
 */
jwt.decode = function jwt_decode(token, key, noVerify, algorithm) {
  // check token
  if (!token) {
    throw new Error('No token supplied');
  }
  // check segments
  var segments = token.split('.');
  if (segments.length !== 3) {
    throw new Error('Not enough or too many segments');
  }

  // All segment should be base64
  var headerSeg = segments[0];
  var payloadSeg = segments[1];
  var signatureSeg = segments[2];

  // base64 decode and parse JSON
  var header = JSON.parse(base64urlDecode(headerSeg));
  var payload = JSON.parse(base64urlDecode(payloadSeg));

  if (!noVerify) {
    if (!algorithm && /BEGIN( RSA)? PUBLIC KEY/.test(key.toString())) {
      algorithm = 'RS256';
    }

    var signingMethod = algorithmMap[algorithm || header.alg];
    var signingType = typeMap[algorithm || header.alg];
    if (!signingMethod || !signingType) {
      throw new Error('Algorithm not supported');
    }

    // verify signature. `sign` will return base64 string.
    var signingInput = [headerSeg, payloadSeg].join('.');
    if (!verify(signingInput, key, signingMethod, signingType, signatureSeg)) {
      throw new Error('Signature verification failed');
    }

    // Support for nbf and exp claims.
    // According to the RFC, they should be in seconds.
    if (payload.nbf && Date.now() < payload.nbf*1000) {
      throw new Error('Token not yet active');
    }

    if (payload.exp && Date.now() > payload.exp*1000) {
      throw new Error('Token expired');
    }
  }

  return payload;
};


/**
 * Encode jwt
 *
 * @param {Object} payload
 * @param {String} key
 * @param {String} algorithm
 * @param {Object} options
 * @return {String} token
 * @api public
 */
jwt.encode = function jwt_encode(payload, key, algorithm, options) {
  // Check key
  if (!key) {
    throw new Error('Require key');
  }

  // Check algorithm, default is HS256
  if (!algorithm) {
    algorithm = 'HS256';
  }

  var signingMethod = algorithmMap[algorithm];
  var signingType = typeMap[algorithm];
  if (!signingMethod || !signingType) {
    throw new Error('Algorithm not supported');
  }

  // header, typ is fixed value.
  var header = { typ: 'JWT', alg: algorithm };
  if (options && options.header) {
    assignProperties(header, options.header);
  }

  // create segments, all segments should be base64 string
  var segments = [];
  segments.push(base64urlEncode(JSON.stringify(header)));
  segments.push(base64urlEncode(JSON.stringify(payload)));
  segments.push(sign(segments.join('.'), key, signingMethod, signingType));

  return segments.join('.');
};

/**
 * private util functions
 */

function assignProperties(dest, source) {
  for (var attr in source) {
    if (source.hasOwnProperty(attr)) {
      dest[attr] = source[attr];
    }
  }
}

function verify(input, key, method, type, signature) {
  if(type === "hmac") {
    return (signature === sign(input, key, method, type));
  }
  else if(type == "sign") {
    return crypto.createVerify(method)
                 .update(input)
                 .verify(key, base64urlUnescape(signature), 'base64');
  }
  else {
    throw new Error('Algorithm type not recognized');
  }
}

function sign(input, key, method, type) {
  var base64str;
  if(type === "hmac") {
    base64str = crypto.createHmac(method, key).update(input).digest('base64');
  }
  else if(type == "sign") {
    base64str = crypto.createSign(method).update(input).sign(key, 'base64');
  }
  else {
    throw new Error('Algorithm type not recognized');
  }

  return base64urlEscape(base64str);
}

function base64urlDecode(str) {
  return Buffer.from(base64urlUnescape(str), 'base64').toString();
}

function base64urlUnescape(str) {
  str += new Array(5 - str.length % 4).join('=');
  return str.replace(/\-/g, '+').replace(/_/g, '/');
}

function base64urlEncode(str) {
  return base64urlEscape(Buffer.from(str).toString('base64'));
}

function base64urlEscape(str) {
  return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
}
});

var jwtSimple = jwt_1;

const db = uniCloud.database();
async function validateToken(token) {
	const userFromToken = JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString());
	const userInDB = await db.collection('usermp').where(userFromToken).get();
	if (userInDB.data.length !== 1) {
		return {
			code: -1,
			errCode: 'TOKEN_INVALID',
			msg: '查无此人'
		}
	}
	const userInfoDB = userInDB.data[0];
	let userInfoDecode;

	userInfoDecode = jwtSimple.decode(token, userInfoDB.tokenSecret);

	function checkUser(userFromToken, userInfoDB) {
		return Object.keys(userFromToken).every(function(item) {
			return userFromToken[item] === userInfoDB[item] && userFromToken[item] === userInfoDecode[item]
		})
	}


	if (userInfoDB.exp > Date.now() && checkUser(userFromToken, userInfoDB)) {
		return {
			code: 0,
			username: userInfoDB.username,
			user_id:userInfoDB._id,
			msg: 'token验证成功'
		}
	}

	if (userInfoDB.exp < Date.now()) {
		return {
			code: -3,
			errCode: 'TOKEN_EXPIRED',
			msg: 'token已失效'
		}
	}

	return {
		code: -2,
		errCode: 'TOKEN_INVALID',
		msg: 'token无效'
	}

}

var validateToken_1 = {
	validateToken
};

/**
 * @description 创建订单
 * @author unhejing (成都-敬宇杰)
 * @date 2020-02-26 上午11:28:02
 */

/**
 * 入参数
{
	"remark":"订单备注",
	"pay_type":0,// 支付方式 0 现金支付 1 微信支付
	"productItemList": [
		{
			"product_id": "238",
			"num": 1
		},
		{
			"product_id": "239",
			"num": 1
		}
	]
}
 * 保存的参数
"data": {
	"_id": "253",
	"address": "2单元9-7",
	"amount_payable": 344.52,
	"consignee_name": "马云",
	"create_time": 1582167318905,
	"link_phone": "17695479404",
	"order_code": "HTZ2019091010003",
	"pay_type": "现金支付",
	"productItemList": [
		{
			"_id": "238",
			"book_price": 78,
			"create_time": 1582168708534,
			"num": 1,
			"order_code": "HTZ2019091010003",
			"primary_img": "https://img13.360buyimg.com/n0/jfs/t1/2105/20/5696/216613/5ba094f4E02210af7/9a61e11b623c5cfc.jpg",
			"product_code": "100000384344",
			"product_id": "00061b1a-6e79-4bc4-b491-3a500f526afc",
			"product_name": "金克里特 特级初榨橄榄油1000ml",
			"sale_unit": "瓶",
			"sum_price": 78
		},
		{
			"_id": "239",
			"book_price": 59.4,
			"create_time": 1582168708534,
			"num": 3,
			"order_code": "HTZ2019091010003",
			"primary_img": "https://img13.360buyimg.com/n0/jfs/t8806/193/753910013/512578/b473c342/59ae65edN87264964.jpg",
			"product_code": "4485343",
			"product_id": "fa8879fc-e3d7-45bb-888f-ac02453d047c",
			"product_name": "和茶原叶 茶叶 花果茶 白桃乌龙茶 调味茶 三角袋泡茶 冷泡茶包 48g",
			"sale_unit": "盒",
			"sum_price": 178.2
		},
		{
			"_id": "240",
			"book_price": 58.32,
			"create_time": 1582168708534,
			"num": 1,
			"order_code": "HTZ2019091010003",
			"primary_img": "http://img13.360buyimg.com/n0/jfs/t8347/104/778706312/519515/a0e2af46/59ae644cN1c58e0be.jpg",
			"product_code": "4485369",
			"product_id": "66a11726-35ab-4ae7-b84c-49136d2d0547",
			"product_name": "和茶原叶 茶叶 花草茶 玫瑰荷叶茶 调味茶 三角袋泡茶包 64g",
			"sale_unit": "盒",
			"sum_price": 58.32
		}
	],
	"real_pay": 344.52,
	"remark": "请下午配送",
	"status": 0,
	"statusDesc": "待付款",
	"update_time": 1582167318905,
	"user_id": "1"
}
 */
const {
	validateToken: validateToken$1
} = validateToken_1;
const db$1 = uniCloud.database();
var main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event:' + event);
	let tokenRes = await validateToken$1(event.token);
	if (tokenRes.code != 0) {
		return tokenRes;
	}
	event.user_id = tokenRes.user_id;
	/**
	 * ******************测试的时候打开该注释start******************
	 */
	// event.user_id = "1";
	// event.remark = "接口测试订单";
	// event.pay_type = 1;
	// event.productItemList = [
	// 		{
	// 			product_id: "007abbbe-11ba-438e-a4e7-2441085784d8",
	// 			num: 1
	// 		},
	// 		{
	// 			product_id: "00f294fb-b972-4271-8bb0-f21292792977",
	// 			num: 2
	// 		}
	// ]
	
	// let addUserInfo = {
	//     guid: "2008017YR51G1XWH",
	//     wx_open_id: "oRrdQt8RirlYXoH60J-2tO39Xhpc",
	//     name: "unhejing",
	//     phone: "17695479404",
	// 	address:"2单元1-1",
	//     photo: "https://unhejing-img.oss-cn-beijing.aliyuncs.com/UTOOLS1581511281662.jpg",
	//     create_time: 1582120959821, // 时间戳 GMT
	//     create_ip: "125.68.18.43" // 注册 ip
	// }
	// let userRes = await db.collection('user').doc(event.user_id).update(addUserInfo);
	// console.log("添加用户信息："+JSON.stringify(userRes))
	/**
	 * ******************测试的时候打开该注释end******************
	 */
	
	if (event.user_id == '' || !event.user_id) {
		return {
			success: false,
			code: -1,
			msg: '参数错误，缺少用户id'
		}
	}
	var randomNum = "";
	for (var i = 0; i < 4; i++) {
		randomNum += Math.floor(Math.random() * 10);
	}
	// 查询用户信息
	let userData = await db$1.collection('user').doc(event.user_id).get();
	if (!userData.data || userData.data.length === 0) {
		return {
			success: false,
			code: -1,
			msg: '查询用户信息出错'
		}
	}
	let userInfo = userData.data[0];
	if(!userInfo.phone || userInfo.phone == ''){
		return {
			success: false,
			code: -1,
			msg: '请设置联系电话'
		}
	}
	if(!userInfo.address || userInfo.address == ''){
		return {
			success: false,
			code: -1,
			msg: '请设置收货地址'
		}
	}
	if(!userInfo.name || userInfo.name == ''){
		return {
			success: false,
			code: -1,
			msg: '请设置收货收货人姓名'
		}
	}
	console.log("下单用户信息："+JSON.stringify(userInfo));

	// 构建订单对象
	let currentTime = new Date().getTime();
	let orderCode = "SQDG" + currentTime + randomNum;
	let orderObj = {
		order_code: orderCode,
		user_id: event.user_id,
		link_phone: userInfo.phone,
		address: userInfo.address,
		consignee_name: userInfo.name,
		amount_payable: 0,
		real_pay: 0,
		status: 0,
		remark: event.remark,
		create_time: currentTime,
		update_time: currentTime
	};
	// 获取商品信息
	console.log("***********************华丽的分割线（获取商品信息）***********************");
	// 获取订单code集合
	let productIds = new Array();
	let productItemList = event.productItemList;
	productItemList.map(o => productIds.push(o.product_id));
	console.log("商品id集合：" + JSON.stringify(productIds));
	// 查询商品信息
	const dbCmd = db$1.command;
	let productList = await db$1.collection('product').where({
		_id: dbCmd.in(productIds)
	}).get();
	console.log("商品查询结果：" + JSON.stringify(productList));
	if(productList.data.length != productIds.length){
		return {
			success: false,
			code: -1,
			msg: '商品信息有误'
		}
	}
	
	// 构建商品信息
	let orderProductArr = productItemList.map(o => {
		let tempObj = productList.data.filter(product=>product._id == o.product_id)[0];
		let sumPrice = (parseFloat(tempObj.book_price) * parseFloat(o.num)).toFixed(2);
		orderObj.amount_payable = parseFloat(orderObj.amount_payable)+parseFloat(sumPrice);
		orderObj.real_pay = parseFloat(orderObj.real_pay)+parseFloat(sumPrice);
		return {
			order_code: orderCode, // string 订单编码
			product_id: o.product_id, // string 商品id（冗余，方便查询商品）
			num: o.num, // int 商品数量
			create_time: currentTime, // 时间戳 GMT, 创建时间
			product_code: tempObj.product_code, // string 商品编码
			product_name: tempObj.product_name, // string 商品名称
			primary_img: tempObj.primary_img, // string 商品主图
			book_price: parseFloat(tempObj.book_price), //double 商品价格
			sum_price: sumPrice, // double 商品合计价格
			sale_unit: tempObj.sale_unit, //string 销售单位
		}
	});
	console.log("构建订单商品："+JSON.stringify(orderProductArr));
	// 保存订单
	const collection = db$1.collection('order'); // 获取表'order'的集合对象
	let res;
	res = await collection.add(orderObj);
	console.log("订单添加响应数据:" + JSON.stringify(res));
	// 保存商品
	for (var i = 0; i < orderProductArr.length; i++) {
		await db$1.collection('order_product').add(orderProductArr[i]);
	}
	if (!res.id) {
		return {
			success: false,
			code: -1,
			msg: '暂无数据'
		}
	}
	
	if (res.id) {
		return {
			success: true,
			code: 0,
			msg: '成功',
			data: {
				id:res.id,
				real_pay:orderObj.real_pay
			}
		}
	}
	return {
		success: false,
		code: -1,
		msg: '服务器内部错误'
	}

};

var createOrder = {
	main: main
};

exports.default = createOrder;
exports.main = main;
