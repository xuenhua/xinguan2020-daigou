import Vue from 'vue'
import store from './store' 
import App from './App'

import Json from './Json' //测试用数据
import data from './data'
import {myMath} from "@/common/mathUtils.js";  // 公用数学计算方法
Vue.prototype.$math = myMath;

import loginMP from "@/common/loginMP.js"  //微信登录公用方法
Vue.prototype.$loginMP = loginMP;

/**
 *  css部分使用了App.vue下的全局样式和iconfont图标，有需要图标库的可以留言。
 *  示例使用了uni.scss下的变量, 除变量外已尽量移除特有语法,可直接替换为其他预处理器使用
 */
const msg = (title, duration=1500, mask=false, icon='none')=>{
	//统一提示方便全局修改
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}
const json = type=>{
	//模拟异步请求数据
	return new Promise(resolve=>{
		setTimeout(()=>{
			resolve(Json[type]);
		}, 500)
	})
}

const network = type => {
	return new Promise(resolve=>{
		setTimeout(()=>{
			resolve(data[type])
		},500)
	})
}

const prePage = ()=>{
	
	// 当前页面栈  是一个数组
	let pages = getCurrentPages();
	// console.log(pages);
	let prePage = pages[pages.length - 2];
	// console.log(prePage);
	// #ifdef H5
	return prePage;
	// #endif
	return prePage.$vm;
}


Vue.config.productionTip = false
Vue.prototype.$fire = new Vue();
Vue.prototype.$store = store;
Vue.prototype.$api = {msg, json, prePage,network};

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()